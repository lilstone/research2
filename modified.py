import collections
import copy
import sys

INF = 1000

#subLoopTarget, subLoopCounter, subLoopNumber, migrationVL, isInMigrationContext, previousVL
specialReg = {'slt':None, 'slc':None, 'sln':None, 'mvl':None, 'imc':False, 'pvl':None}

class CPU:

    def __init__(self, vl, mem):
        self.vl = vl
        self.mem = mem
        self.scalarReg = collections.defaultdict(lambda : 0)
        self.zReg = collections.defaultdict(lambda : [0] * vl)
        self.pReg = collections.defaultdict(lambda : [0] * vl)
        self.lastPredicate = None
        self.hwContext = (0, self.scalarReg, self.zReg, self.pReg, self.lastPredicate)
        self.prevHwContext = None
    
    def migration(self, program, prevVL, prevHwContext):
        pc, scalarReg, zReg, pReg, lastPredicate = prevHwContext
        self.prevHwContext = prevHwContext
        self.pc = pc
        self.program = copy.deepcopy(program)
        self.scalarReg = copy.deepcopy(scalarReg)
        self.lastPredicate = lastPredicate

        specialReg['imc'] = True
        specialReg['mvl'] = self.vl
        specialReg['slt'] = pc
        specialReg['slc'] = 0
        specialReg['sln'] = prevVL / self.vl 
        specialReg['pvl'] = prevVL

        if self.vl < prevVL:
            shortLength = self.vl
        else:
            shortLength = prevVL

        for k in zReg.keys():
            for t in range(shortLength):
                self.zReg[k][t] = zReg[k][t]

        for k in pReg.keys():
            for t in range(shortLength):
                self.pReg[k][t] = pReg[k][t]
        

    def run(self, timer, verbose = 0):
        instStream, labelDict = self.program

        pc = self.pc
        mem = self.mem
        scalarReg = self.scalarReg
        zReg = self.zReg
        pReg = self.pReg
        vl = self.vl
        count = -1

        while True:  
            count += 1
            #print count, pc, instStream[pc]
            if count == timer:
                break

            i = instStream[pc]
            if i[0] == "ret":
                break

            elif i[0] == "mov":
                if i[1][0] == "x":
                    scalarReg[i[1]] = int(i[2])
                if i[1][0] == "z":
                    for t in range(vl):
                        zReg[i[1][0:2]][t] = int(i[2])

            elif i[0] == "b":
                pc = labelDict[i[1]]
                continue

            elif i[0] == "ldrsw":
                scalarReg[i[1]] = mem[scalarReg[i[2]]]

            elif i[0] == "strsw": #arbitrary inst
                mem[scalarReg[i[2]]] = scalarReg[i[1]]

            elif i[0] == "incd" or i[0] == "incw":
                if specialReg['imc'] == True:
                    if specialReg['pvl'] < self.vl:
                        scalarReg[i[1]] += specialReg['pvl']
                    specialReg['imc'] = False
                else:
                    scalarReg[i[1]] += vl

            elif i[0] == "ld1rd":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        zReg[i[1][0:2]][t] = mem[scalarReg[i[3]]]

            elif i[0] == "whilelt":
                self.lastPredicate = i[1][0:2]
                for t in range(vl):
                    if scalarReg[i[2]] + t < scalarReg[i[3]]:
                        pReg[i[1][0:2]][t] = 1
                    else:
                        pReg[i[1][0:2]][t] = 0

            elif i[0] == "ld1d" or i[0] == "ld1w":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        zReg[i[1][0:2]][t] = mem[scalarReg[i[3]] + scalarReg[i[4]]+ t]
                    elif i[2][3] == "z":
                        zReg[i[1][0:2]][t] = 0
            
            elif i[0] == "fmla":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        zReg[i[1][0:2]][t] += zReg[i[3][0:2]][t] * zReg[i[4][0:2]][t]
            
            elif i[0] == "st1d" or i[0] == "st1w":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        mem[scalarReg[i[3]] + scalarReg[i[4]] + t] = zReg[i[1][0:2]][t]
            
            elif i[0] == "b.first":
                if specialReg['imc'] == True:
                    if specialReg['slt'] < labelDict[i[1]]:
                        specialReg['imc'] = False
                    else:
                        specialReg['slc'] += 1
                        if specialReg['slc'] == specialReg['sln']:
                            specialReg['imc'] = False
                        else:
                            if specialReg['pvl'] > self.vl:
                                pc = specialReg['slt']
                                oldzReg = self.prevHwContext[2]
                                oldpReg = self.prevHwContext[3]

                                for k in oldzReg.keys():
                                    for t in range(self.vl):
                                        zReg[k][t] = oldzReg[k][specialReg['slc'] * self.vl + t]

                                for k in oldpReg.keys():
                                    for t in range(self.vl):
                                        pReg[k][t] = oldpReg[k][specialReg['slc'] * self.vl + t]
                                continue

                if specialReg['imc'] == False and pReg[self.lastPredicate][0] == 1:
                    pc = labelDict[i[1]]
                    continue

            elif i[0] == "add":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        zReg[i[1][0:2]][t] = zReg[i[3][0:2]][t] + zReg[i[4][0:2]][t]

            elif i[0] == "cmpgt":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        if zReg[i[3][0:2]][t] > int(i[4]):
                            pReg[i[1][0:2]][t] = 1
                        else:
                            pReg[i[1][0:2]][t] = 0
                    elif i[2][3] == "z":
                        pReg[i[1][0:2]][t] = 0

            elif i[0] == "cmpne":
                for t in range(vl):
                    if pReg[i[2][0:2]][t] == 1:
                        if zReg[i[3][0:2]][t] != int(i[4]):
                            pReg[i[1][0:2]][t] = 1
                        else:
                            pReg[i[1][0:2]][t] = 0
                    elif i[2][3] == "z":
                        pReg[i[1][0:2]][t] = 0

            elif i[0] == "ptrue":
                for t in range(vl):
                    pReg[i[1][0:2]][t] = 1

            elif i[0] == "saddv":
                for t in range(vl):
                    if pReg[i[2]][t] == 1:
                        scalarReg[i[1]] += zReg[i[3][0:2]][t]

            elif i[0] == "label":
                pass
            
            else:
                print "Unsupported instruction [" + i[0] + "]"
                sys.exit()
            
            if verbose == 1:
                print "inst " + str(pc) + ", [" + i[0] + "]"
                print mem
                self.regPrint()

            pc = pc + 1

        self.program = (instStream, labelDict)
        self.hwContext = (pc, self.scalarReg, self.zReg, self.pReg, self.lastPredicate)


    def regPrint(self):
        print self.scalarReg
        print self.pReg
        print self.zReg
        print "#######"

def main(filename, VL1, VL2, stepn, runtype):
    instStream = []
    labelDict = {}
    scalarReg = collections.defaultdict(lambda : 0)
    specialReg = {}
    mem = [-1] * 50
    def testOne(oriVL, targetVL, count, verbose = 0):
        memcopy = copy.deepcopy(mem)
        origin = CPU(oriVL, memcopy)
        target = CPU(targetVL, memcopy)
        
        if verbose == 1:
            print "   << now run with VL " + str(oriVL) + " >>"
        origin.migration((instStream, labelDict), oriVL, (0, scalarReg, {}, {}, None))
        origin.run(count, verbose = verbose)

        if verbose == 1:
            print "\n   << now run with VL " + str(targetVL) + " >>"
        target.migration(origin.program, oriVL, origin.hwContext)
        target.run(INF, verbose = verbose)

        if reference != memcopy:
            print "  Differs at " + str(count)
            print "  ", memcopy 


    def testAll(oriVL, targetVL, verbose = 0):
        for i in range(INF):
            testOne(oriVL, targetVL, i, verbose)
            

    def decoding(inst, instNum):
        nowhite = ''.join(filter(lambda c: c not in "\n\r", inst))
        if nowhite[-1] == ":": #label
            instStream.append(("label", nowhite[0:-1]))
            labelDict[nowhite[0:-1]] = instNum
            return
        
        stripped = ''.join(filter(lambda c: c not in "[],#\n", nowhite))
        token = stripped.split()
        instStream.append(tuple(token))

    def inputProcessing():
        mem_bottom = 0 
        variables = {}
        f = open(filename, "r")
        i = 0
        while True:
            line = f.readline()
            if line[0] == "#": break
            decoding(line, i)
            i = i + 1

        while True:
            memInfo = f.readline()
            if memInfo[0] == "#": break
            token = memInfo.replace("=", " ").split()
            if token[1].count('[') == 0:
                mem[mem_bottom] = int(token[1])
                variables[token[0]] = mem_bottom
                mem_bottom += 1
            
            else:
                token = memInfo.replace("=", " ").replace("[", " ").replace("]", " ").replace(",", " ").split()
                for i in range(1, len(token)):
                    mem[mem_bottom] = int(token[i])
                    if i == 1:
                        variables[token[0]] = mem_bottom
                    mem_bottom += 1


        while True:
            regInfo = f.readline()
            if not regInfo: break
            token = regInfo.replace("=", " ").split()
            if token[1].count('&') == 0:
                scalarReg[token[0]] = int(token[1])
            else:
                scalarReg[token[0]] = variables[token[1][1:]]

            f.close

    inputProcessing()   
    # ensure the same result in the two cores
    memcopy1 = copy.deepcopy(mem)
    memcopy2 = copy.deepcopy(mem)
    little = CPU(VL1, memcopy1)
    big = CPU(VL2, memcopy2)
    little.migration((instStream, labelDict), VL1, (0, scalarReg, {}, {}, None))
    little.run(INF)
    big.migration((instStream, labelDict), VL2, (0, scalarReg, {}, {}, None))
    big.run(INF)
    
    if memcopy1 != memcopy2:
        print "!!!!seperate results differs!!!!"

    reference = memcopy1

    print "> Reference memory"
    print " ", reference

    if runtype == 0:
        if VL1 > VL2:
            littleVL = VL2
            bigVL = VL1
        else:
            littleVL = VL1
            bigVL = VL2

        print "> Core size :", littleVL, "/", bigVL

        # find error situation, big->little
        print "\nTest for big -> little ----------"
        testAll(bigVL, littleVL, verbose = 0)

        # find error situation, big->little
        print "\nTest for little -> big ----------"
        testAll(littleVL, bigVL, verbose = 0)

    elif runtype == 1:
        testOne(VL1, VL2, stepn, verbose = 1)


if __name__ == "__main__":
    if sys.argv[1] == '-help':
        print 'Usage'
        print ' filename -all VL1 VL2'
        print ' filename -one VL1 VL2 stepn'
    elif sys.argv[2] == '-all':
        main(sys.argv[1], int(sys.argv[3]), int(sys.argv[4]), None, 0)
    elif sys.argv[2] == '-one':
        main(sys.argv[1], int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), 1)
